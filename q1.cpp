//This program uses the shooting method to solve the ODEs in question 1

#include <iostream>
#include <fstream>
#include <limits>
#include <iomanip>
#include <cmath>
#include <vector>
#include <stdexcept>

#include "cpp_brent.h" //Sourced from https://github.com/duncanmcnae/cpp_brent


using namespace std;

vector<double> f(  const double x, const vector<double> y )
{
    int N = y.size();
    vector<double> yp(N);
    yp[0] = -(y[0]*y[0])-y[1]  ;
    yp[1] = (5*y[0])-y[1];
    return yp;
}

vector<double> RK( const double x, const vector<double> y, const double h)
{
    int N = y.size();
    
    vector<double> ynext(N);
    vector<double> k1(N);
    vector<double> k2(N);
    vector<double> k3(N);
    vector<double> k4(N);
    
    vector<double> k1y(N);
    vector<double> k2y(N);
    vector<double> k3y(N);
    
    int i;
    
    // Calculate k1 for all equations
    k1 = f( x, y );
    for (i = 0; i < N; i++) {
        k1y[i] = y[i] + k1[i] * h/2.;
    }
    
    // Calculate k2 for all equations
    k2 = f( x + h/2., k1y );
    for (i = 0; i < N; i++){
        k2y[i] = y[i] + k2[i] * h/2.;
    }
    
    // Calculate k3 for all equations
    k3 = f( x + h/2., k2y );
    for (i = 0; i < N; i++) {
        k3y[i] = y[i] + k3[i] * h;
    }
    
    // Calculate k4 for all equations
    k4 = f( x + h, k3y );
    
    // New y - Synthesis
    for (int i = 0; i < N; i++) {
        ynext[i] = y[i] + ( k1[i] + 2.*k2[i] + 2.*k3[i] + k4[i] ) * h/6.;
    }
    
    return ynext;
}

//This function takes the initial condition y2(0) as its argument; it consequently solves the given initial value problem
double RK(double X)
{
    
    vector<double> y0(2);
    y0[0]=1.5; y0[1]=X; //Modify initial conditions here
    
    int N=y0.size();
    const double eps = 1.e-6;
    double deltax = 1.e-2;
    double x = 0.0;                // Modify initial x here
    const double range = 10.0;      // Modify final x here
    vector<double> y1(N), y2(N), ynext(N); // Creates number of steps between x and range given deltax
    vector<double> D1(N);
    double Dmax;
    
    //This loop performs the step doubling process whilst calling the Runge-Kutta method
    while( x < range )
    {
        // Calculates y1 for step doubling
        y1 = RK( x, y0, deltax );
        
        // Calculates y2 for step doubling method, this loop goes halfway to x+deltax
        y2 = RK( x, y0, deltax/2.0 );
        
        // y2 for step doubling method, this loop completes the jump to x+deltax
        y2 = RK(x+deltax/2., y2, deltax/2.0 );
        
        // Calculate error
        Dmax = 0; // maximum error
        for (int i = 0; i < N; i++) {
            D1[i] = y2[i] - y1[i];
            // Calculate the maximum error
            if ( abs(D1[i]) >= Dmax ) {
                Dmax = abs(D1[i]);
            }
        }
        
        if( Dmax > eps )
        {
            deltax = deltax / 2.;
        }
        else
        {
            for(int j=0; j<N ; j++) //Calculates y_n+1 from page 54 in the lecture slides
            {
                ynext[j] = y2[j] + D1[j]/15.;
            }
            
            x = x + deltax; //Sets x to the next point in the iteration
            
            // Check if error is too small
            if( Dmax < eps/10. ){
                deltax = deltax * 2.;
            }
            
            // Check if new step is larger than range
            if( x + deltax - range >= 1.e-6 )
            {
                deltax = range - x;
            }
        }
        
        for(int j=0; j < N; j++) //sets y0 array to be y_n+1
        {
            y0[j] = ynext[j];
        }
        
    }
    
    if (ynext[0]!=ynext[0]) {
        throw runtime_error("RK return nan");
    }
    
    return ynext[0]-0.0; //Alter 0.0 to a different boundary condition (if so desired) here; this returns the difference between the y1(10) associated with the supplied y2(0) and the actual y1(10) provided by the boundary conditions
};

double RKWRITE(double X) //This function writes the results of RK45 to "data.txt"
{
    
    vector<double> y0(2);
    y0[0]=1.5; y0[1]=X; //Modify initial conditions here
    
    int N=y0.size();
    const double eps = 1.e-6;
    double deltax = 1.e-2;
    double x = 0.0;                // Modify initial x here
    const double range = 10.0;      // Modify final x here
    vector<double> y1(N), y2(N), ynext(N); // Creates number of steps between x and range given deltax
    vector<double> D1(N);
    double Dmax;
    
    ofstream data("data.txt");
     data.precision( std::numeric_limits<double>::digits10 );
    
    
    // Write intiial conditions to data.txt
    data << "x\ty[i]" << endl;
     data << x << "\t";
     for( int i=0; i < N; i++ )
     {
     data << y0[i] << "\t";
     }
     data << endl;
    
    
    //This loop performs the step doubling process whilst calling the Runge-Kutta method
    while( x < range )
    {
        // Calculates y1 for step doubling
        y1 = RK( x, y0, deltax );
        
        // Calculates y2 for step doubling method, this loop goes halfway to x+deltax
        y2 = RK( x, y0, deltax/2.0 );
        
        // y2 for step doubling method, this loop completes the jump to x+deltax
        y2 = RK(x+deltax/2., y2, deltax/2.0 );
        
        // Calculate error
        Dmax = 0; // maximum error
        for (int i = 0; i < N; i++) {
            D1[i] = y2[i] - y1[i];
            // Calculate the maximum error
            if ( abs(D1[i]) >= Dmax ) {
                Dmax = abs(D1[i]);
            }
        }
        
        if( Dmax > eps )
        {
            deltax = deltax / 2.;
        }
        else
        {
            for(int j=0; j<N ; j++) //Calculates y_n+1 from page 54 in the lecture slides
            {
                ynext[j] = y2[j] + D1[j]/15.;
            }
            
            x = x + deltax; //Sets x to the next point in the iteration
            
            data << x << "\t";
             for(int j=0; j < N; j++) //Writes the output data to erf2.txt in the following format: x        y[0]      y[1]     ...
             {
             data <<  ynext[j] << "\t";
             }
             data << endl;
            
            // Check if error is too small
            if( Dmax < eps/10. ){
                deltax = deltax * 2.;
            }
            
            // Check if new step is larger than range
            if( x + deltax - range >= 1.e-6 )
            {
                deltax = range - x;
            }
        }
        
        for(int j=0; j < N; j++) //sets y0 array to be y_n+1
        {
            y0[j] = ynext[j];
        }
        
    }
    
    if (ynext[0]!=ynext[0]) {
        throw runtime_error("RK return nan");
    }
    
    return ynext[0]-0.0;
};

int main() {
    //This function returns y1(10) as a function of the supplied y2(0)
    auto g = [](double x) {
        double y = RK(x);

        return y;
    };

    // a and b are the lower and upper bounds for the initial guess of y2(0)
    
    double a = -7.0;  // lower bound of search interval
    double b = -5.0;     // upper bound of search interval
    double tolerance = 0.0001;  // desired solution tolerance
    double max_iterations = 100;

    
    double root = brents_method(g, a, b, tolerance, max_iterations);

    cout << "Root is: " << root << endl;  // produces 'root' of RK45
    
    RKWRITE(root); //This writes the solution of the 'root' deduced to "data.txt"
    
}
