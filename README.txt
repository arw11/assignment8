Brent's method (cpp_brent.h) was sourced from https://github.com/duncanmcnae/cpp_brent

For question 1:

type " g++ -std=c++11 q1.cpp -o q1 " to compile
type " ./q1 " to run
x and y points are output to data.txt